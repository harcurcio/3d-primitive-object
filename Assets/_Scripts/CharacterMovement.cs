﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public CharacterController controller; //need this to move char
    public float speed = 12f; //walking speed

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal"); //preprogrammed unity a&d movement
        float z = Input.GetAxis("Vertical"); //same as x

        Vector3 move = transform.right * x + transform.forward * z; //moves in regards to players location
        controller.Move(move * speed * Time.deltaTime); //delta time so its frame-rate independ
    }
}
