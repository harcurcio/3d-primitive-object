﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Correct : MonoBehaviour
{
    public void SceneLoad(int sceneIndex)
    {
        Debug.Log("back to main scene");
        SceneManager.LoadScene(sceneIndex); //go back to main game when correct
    }
}
