﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetFollow : MonoBehaviour
{
    public GameObject player;
    public GameObject cat;
    public float catDistance; //distance of cat to player
    public float maxDistance = 3; //farthest away cat can get
    private float followSpeed;
    public RaycastHit shot; //shooting raycast towards player

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform); //makes cat look at player
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out shot)) //define shooting raycast out from cat to player
        {
            catDistance = shot.distance; //distance of ray shot is how far the cat is
            if(catDistance >= maxDistance) //if cat gets too far make it move to player
            {
                followSpeed = 0.1f; //speed of cat
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, followSpeed); //moves cat to player
            }
            else
            {
                followSpeed = 0; //cat doesnt move if its as close to player as can be
            }
        }
    }
}
